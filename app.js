/* Let's import some stuff. */
const express = require('express')
const app = express()

const helmet = require('helmet');
const bodyParser = require("body-parser");
const morgan = require('morgan');

const middleware = [
  helmet(),
  morgan('dev'),
  express.static('public'),
  bodyParser.urlencoded({
    extended: true
  }),
  bodyParser.json()
]

app.set('view engine', 'ejs'); // set up ejs for templating
app.use(middleware);

const {
  generateJson,
  generateJsonError
} = require("./generators.js")
/* I want a config file. They are cool. */
const config = require("./config.json")

/*
  HERE BEGINS THE FUNCTIONING PART
*/


// Allows loading stuff in the parent dir

/* GET functions for different stuff */
app.get('/get/random', (req, res) => {
  res.json(generateJson("random", "random", "random"));
})

app.get('/', function (req, res) {
  res.render('index.ejs', {
    generateJson: generateJson,
    random: true
  });
});

app.get('/api', function (req, res) {
  res.render('apidocs.ejs', {
    generateJson: generateJson,
  });
});

app.get('/app', function (req, res) {
  res.render('app.ejs', {});
});

app.get('/playground', function (req, res) {
  res.render('playground.ejs', {
    distro: null,
    adj: null,
    reason: null,
    generateJson: generateJson
  });
});

app.post("/playground", function (req, res) {
  res.render("playground.ejs", {
    distro: req.body.params.distro,
    adj: req.body.params.adj,
    reason: req.body.params.reason,
    generateJson: generateJson
  })
});

app.get("/get/:distro?/:adj?/:reason?", (req, res) => {
  res.json(generateJson(req.params.distro, req.params.adj, req.params.reason))
})

app.get("/:distro?/:adj?/:reason?", function (req, res) {
  res.render('index.ejs', {
    generateJson: generateJson,
    random: false,
    distro: req.params.distro,
    adj: req.params.adj,
    reason: req.params.reason
  });
});

/* STARTS LISTENING FOR REQUESTS */
app.listen(config.port, () => {
  console.log(`Started listening on port ${config.port}\n`)
})
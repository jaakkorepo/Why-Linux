/* Getting data files */
const distro = require("./data/distro.json")
const adj = require("./data/adj.json")
const reason = require("./data/reason.json")

function generateJson(distroChoice, adjChoice, reasonChoice) {

  if (!(distroChoice && adjChoice && reasonChoice)) {
    return generateJsonError(404, "Missing arguments.")
  } else {


    if (distroChoice === "random") {
      distroChoice = Math.floor(Math.random() * distro.length)
    } else {
      distroChoice = parseInt(distroChoice)
    }

    if (adjChoice === "random") {
      adjChoice = Math.floor(Math.random() * adj.length)
    } else {
      adjChoice = parseInt(adjChoice)
    }

    if (reasonChoice === "random") {
      reasonChoice = Math.floor(Math.random() * reason.length)
    } else {
      reasonChoice = parseInt(reasonChoice);
    }

    if (!(distro[distroChoice] && adj[adjChoice] && reason[reasonChoice])) {
      return generateJsonError(400, "Value too big or not a number")
    } else {


      const resultStr = `${distro[distroChoice]} is ${adj[adjChoice]}, because ${reason[reasonChoice]}.`;

      const jsonObject = {
        "status": 200,
        "message": resultStr,
        "url": `/get/${distroChoice}/${adjChoice}/${reasonChoice}`,
        "permaLink": `/${distroChoice}/${adjChoice}/${reasonChoice}`,
        "parts": {

          "distro": {
            "id": distroChoice,
            "string": distro[distroChoice]
          },
          "adj": {
            "id": adjChoice,
            "string": adj[adjChoice]
          },
          "reason": {
            "id": reasonChoice,
            "string": reason[reasonChoice]
          }

        }
      };

      return jsonObject;
    }
  }
}

const generateJsonError = (errorCode, errorMessage) => {
  return {
    "status": errorCode,
    "message": errorMessage
  }
}

exports.generateJsonError = generateJsonError;
exports.generateJson = generateJson;